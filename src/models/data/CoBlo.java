package models.data;

import interfaces.MatrixElement;

public class CoBlo implements MatrixElement {

    private int posX;
    private int posY;
    private int level;
    private int health;

    public CoBlo(int posX, int posY, int level, int health) {
        setPosX(posX);
        setPosY(posY);
        setLevel(level);
        setHealth(health);
    }

    public int getPosX() {
        return posX;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }
}
