package controller;

import models.data.Matrix;

public class GameController {

    private int round, maxRounds, width, height;
    private DataModelController dataModelController;
    private ViewController viewController;

    private static int DEFAULT_MAX_ROUNDS = 50;
    private static int DEFAULT_WIDTH = 8;
    private static int DEFAULT_HEIGHT = 8;

    public GameController(int maxRounds, int width, int height) {
        this.setMaxRounds(maxRounds);
        this.setWidth(width);
        this.setHeight(height);

        this.dataModelController = new DataModelController(width, height);
        this.viewController = new ViewController(width, height);
    }

    /*
     *    Getters
     */
    public int getRound() {
        return this.round;
    }

    public int getMaxRounds() {
        return this.maxRounds;
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

    /*
     *  Setters
     */

    public void setMaxRounds(int maxRounds) {
        if (this.maxRounds == 0 && maxRounds > 0) {
            this.maxRounds = maxRounds;
        } else {
            this.maxRounds = DEFAULT_MAX_ROUNDS;
        }
    }

    public void setWidth(int width) {
        if (this.width == 0 && width > 0) {
            this.width = width;
        } else {
            this.width = DEFAULT_WIDTH;
        }
    }

    public void setHeight(int height) {
        if (this.height == 0 && height > 0) {
            this.height = height;
        } else {
            this.height = DEFAULT_HEIGHT;
        }
    }

    /*
     * Methods
     */

    /**
     * This method is the place where each cycle is started until the maximum number of rounds is reached. It calls the
     * DataModelController in order to update the data model of the underlying Matrix. Afterwards
     * changes are sent to the view controller in order to respond to the freshly made changes.
     */
    public void startGame() {
        do {
            this.dataModelController.doNewRound();
            Matrix matrix = dataModelController.getMatrix();
            this.viewController.refreshView(matrix);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.round++;
        } while (round < this.maxRounds);
    }
}
