package interfaces;

public interface MatrixElement {
    public int getPosX();

    public void setPosX(int posX);

    public int getPosY();

    public void setPosY(int posY);

}
