package models.view;

import models.data.Matrix;

import javax.swing.*;

public class MainFrame {

    private JFrame jFrame;
    private JPanel jPanel;
    private MatrixPanel matrixPanel;
    private int width, height;
    private boolean isInitial = true;

    public MainFrame(int width, int height) {
        this.width = width;
        this.height = height;
        this.matrixPanel = new MatrixPanel(this.width, this.height);
    }

    public void updateView(Matrix matrix) {

        // If called for the first time, config the frame
        if (isInitial) {
            this.jFrame = new JFrame("CoBlo Universe");
            this.jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        }
        if (this.jPanel != null) {
            this.jFrame.remove(this.jPanel);
        }
        this.matrixPanel.refreshMainPanel(matrix.getMatrixElements());
        this.jPanel = this.matrixPanel.getMainPanel();
        this.jFrame.add(this.jPanel);
        this.jFrame.validate();
        this.jFrame.repaint();

        // Also necessary config if the view is updated for the first time
        if (isInitial) {
            this.jFrame.setSize(600, 600);
            this.jFrame.setLocationRelativeTo(null);
            this.jFrame.setVisible(true);
            this.isInitial = false;
        }
    }
}
