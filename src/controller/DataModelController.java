package controller;

import interfaces.MatrixElement;
import models.data.CoBlo;
import models.data.Coordinate;
import models.data.Matrix;
import util.GameUtil;

public class DataModelController {

    private Matrix matrix;

    private int width;
    private int height;

    public static int MAX_NUMBER_OF_SPAWN_ATTEMPTS = 10;

    public DataModelController(int width, int height) {
        this.width = width;
        this.height = height;
        this.matrix = new Matrix(width, height);
    }

    public Matrix getMatrix() {
        return matrix;
    }

    /*
     * Methods
     */

    public void doNewRound() {
        this.doSpawn();
        // TODO: doMove, doAttack, doMultiply
    }

    /**
     * Return a fresh CoBlo entity
     *
     * @param posX
     * @param posY
     * @return
     */
    private MatrixElement getNewCoBlo(int posX, int posY) {
        int level = 0;
        int health = 10;
        return new CoBlo(posX, posY, level, health);
    }


    /**
     * The method spawns a random number of CoBlos in the Matrix. This number may be in a range of 0 and
     * numberOfMatrixCells/10 elements.
     */
    public void doSpawn() {

        int tmp = (int)Math.ceil((this.height * this.width) / 30);
        int maxSpawnNumber = tmp > 1 ? GameUtil.getRandomNumberBetween(0, tmp) : GameUtil.getRandomNumberBetween(0, 1);

        for (int i = 0; i < maxSpawnNumber; i++) {

            int attemptNumber = 0;
            boolean isSpawnInvalid;
            Coordinate coordinate;

            // Try to get free coordinates for a new CoBlo as long as we are underneath a maximum number of attempts
            do {
                coordinate = GameUtil.getRandomCoordinates(this.width, this.height);
                attemptNumber++;
                isSpawnInvalid = attemptNumber > MAX_NUMBER_OF_SPAWN_ATTEMPTS;
            } while (isSpawnInvalid || !this.matrix.isFieldEmpty(coordinate.getPosX(), coordinate.getPosY()));

            // If we have no valid coordinates, spawn no CoBlo
            if (!isSpawnInvalid) {
                MatrixElement coBlo = this.getNewCoBlo(coordinate.getPosX(), coordinate.getPosY());
                this.matrix.setMatrixElement(coordinate.getPosX(), coordinate.getPosY(), coBlo);
            }
        }
    }
}
