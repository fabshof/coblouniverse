package models.view;

import interfaces.MatrixElement;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class MatrixPanel {

    private int rows;
    private int columns;
    private int width, height;
    private GridLayout mainGrid;
    private JPanel mainPanel;
    private ArrayList<JPanel> panelList = new ArrayList<JPanel>();


    public MatrixPanel(int width, int height) {
        setRows(width);
        setColumns(height);
        this.mainGrid = new GridLayout(width, height);
    }

    /*
     * Setter
     */

    public void setRows(int rows) {
        this.rows = rows;
    }

    public void setColumns(int columns) {
        this.columns = columns;
    }

    /*
     * Getter
     */

    public JPanel getMainPanel() {
        return mainPanel;
    }

    /*
     * Methods
     */

    public void refreshMainPanel(MatrixElement[][] matrixElements) {

        this.mainPanel = new JPanel(this.mainGrid);
        ArrayList<JPanel> panelList = new ArrayList<JPanel>();

        for (int i = 0; i < matrixElements.length; i++) {
            for (int j = 0; j < matrixElements[i].length; j++) {

                MatrixElement matrixElement = matrixElements[i][j];
                JPanel jPanel = new JPanel();
                jPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));

                // If the matrix cell contains a MatrixElement, paint the cell
                if (matrixElement != null) {
                    jPanel.setBackground(new Color(160, 200, 35));
                }
                panelList.add(jPanel);
            }
        }
        // Add the single panels to the view
        for (JPanel panelElement : panelList) {
            this.mainPanel.add(panelElement);
        }
    }
}
