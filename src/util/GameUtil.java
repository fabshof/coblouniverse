package util;

import models.data.Coordinate;
import models.data.Matrix;

import java.util.concurrent.ThreadLocalRandom;

public class GameUtil {

    public GameUtil() {}

    /**
     * Returns a random number between the inclusive min and max values
     *
     * @param min
     * @param max
     * @return
     */
    public static int getRandomNumberBetween(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max + 1);
    }

    /**
     * Return random coordinates inside a matrix with the given width and height. The coordinates start with 0 and end
     * with max - 1.
     *
     * @param width
     * @param height
     * @return
     */
    public static Coordinate getRandomCoordinates(int width, int height) {
        int posX = getRandomNumberBetween(0, width - 1);
        int posY = getRandomNumberBetween(0, height - 1);
        return new Coordinate(posX, posY);
    }

    public static Matrix getDiffMatrix (Matrix matrix1, Matrix matrix2) {
        // TODO: Return diff matrix
        return null;
    }
}
