package models.data;

import interfaces.MatrixElement;

import java.util.ArrayList;
import java.util.List;

public class Matrix {

    static final int DEFAULT_WIDTH = 4;
    static final int DEFAULT_HEIGHT = 4;

    private int width;
    private int height;
    private MatrixElement[][] matrixElements;

    public Matrix(int width, int height) {
        this.width = width;
        this.height = height;

        this.matrixElements = new MatrixElement[width][height];

        // Init the values with null
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                matrixElements[x][y] = null;
            }
        }
    }

    /*
     * Getter
     */
    public MatrixElement getMatrixElement(int posX, int posY) {
        return matrixElements[posX][posY];
    }

    public MatrixElement[][] getMatrixElements() {
        return matrixElements;
    }

    /*
     * Methods
     */

    public boolean isFieldEmpty(int posX, int posY) {
        return this.matrixElements[posX][posY] == null;
    }


    public void setMatrixElement(int posX, int posY, MatrixElement matrixElement) {
        this.matrixElements[posX][posY] = matrixElement;
    }

    public void deleteMatrixElement(int posX, int posY) {
        this.setMatrixElement(posX, posY, null);
    }
}
