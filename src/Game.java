import controller.GameController;

public class Game {

    private int width, height, maxRounds;
    private GameController gameController;

    public Game(int maxRounds, int width, int height) {
        this.width = width;
        this.height = height;
        this.maxRounds = maxRounds;
        this.gameController = new GameController(maxRounds, width, height);
        gameController.startGame();
    }
}
