package controller;

import models.data.Matrix;
import models.view.MainFrame;

public class ViewController {

    private int width;
    private int height;
    private MainFrame mainFrame;

    public ViewController(int width, int height) {
        this.width = width;
        this.height = height;
        this.mainFrame = new MainFrame(width, height);
    }

    /*
     * Methods
     */

    public void refreshView(Matrix matrix) {
        this.mainFrame.updateView(matrix);
    }
}
